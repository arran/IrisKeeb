# Iris Rev.4 Keymap

My personal keymap for my split-ortho keyboard.

## Firmware

The Iris Rev.4 supports [QMK Firmware](https://github.com/qmk/qmk_firmware/tree/master/keyboards/keebio/iris).

## VIA

This keymap can be imported with [VIA](https://github.com/the-via/releases)
